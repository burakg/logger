<?php

/**
 * ION Logger class
 *
 * @author Burak Mehmet Gurbuz
 */

namespace burakg\logger;
use burakg\ion;
use burakg\ion\dbBase\dbConn;

class logger {

	protected function __construct() {}

	public static function log($wrapper,$code,$message,$table,$id){
		$db = dbConn::get()->connect('front');

		$sql = $db->prepare("INSERT INTO `logs` (`wrapper`,`code`,`message`,`content_table`,`content_id`,`userdata`) VALUES(?,?,?,?,?,?)");
		try{
			$ex = $sql->execute([$wrapper,$code,$message,$table,(int)$id,json_encode(ion\front\auth::get()->get_user())]);
		}catch (\PDOException $e){
			echo $e->getCode();
		}
	}

	/**
	 * @param int $start
	 * @param int $offset
	 * @return array
	 */
	public static function read_log($start=0,$offset=5){
		$limit = $start+$offset;
		$db = dbConn::get()->initConns()->connect('front');
		$sql = $db->prepare("SELECT * FROM logs ORDER BY added_date DESC LIMIT {$start}, {$limit}");

		$sql->execute();
		return $sql->fetchAll();
	}
}